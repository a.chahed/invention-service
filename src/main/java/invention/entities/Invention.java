package invention.entities;

import invention.utils.InventionIdGenerator;

import java.util.ArrayList;
import java.util.List;

public class Invention implements Comparable<Invention> {

    private final Integer Id;
    private Integer date;
    private String name;
    private String inventor;
    private String origin;
    private String site;
    private List<String> tags;

    public Integer getId() {
        return Id;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInventor() {
        return inventor;
    }

    public void setInventor(String inventor) {
        this.inventor = inventor;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags.clear();
        this.tags.addAll(tags);
    }

    public Invention() {
        Id = InventionIdGenerator.nextValue();
        tags = new ArrayList<>();
    }

    @Override
    public int compareTo(Invention o) {
        if (this.getDate().equals(o.getDate())) {
            return this.getName().compareTo(o.getName());
        }
        return this.getDate().compareTo(o.getDate());
    }

    @Override
    public String toString() {
        return "Invention{" +
                "Id=" + Id +
                ", date=" + date +
                ", name='" + name + '\'' +
                '}';
    }
}
