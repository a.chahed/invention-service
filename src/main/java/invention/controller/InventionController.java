package invention.controller;

import invention.entities.Invention;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class InventionController {

    public static final String PARAM_INVENTION_ID = "id";
    public static final String PARAM_INVENTION_TAG = "tag";
    public static final String RESPONSE_MSG_SUCCESS = "Action succeeded";
    public static final String RESPONSE_MSG_FAIL = "Action failed";

    protected static HashSet<Invention> inventions = new HashSet<>();

    @GetMapping(value = "/inventions")
    public ResponseEntity<List<Invention>> getInventions() {
        List<Invention> resp = new ArrayList<>(inventions);
        Collections.sort(resp);
        return new ResponseEntity<>(resp, HttpStatus.OK);
    }

    @GetMapping(value = "/inventions/{id}")
    public ResponseEntity<Invention> getOneInventionById(@PathVariable(PARAM_INVENTION_ID) Integer id) {
        Invention i = findById(id);
        if (i != null)
            return new ResponseEntity<>(i, HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/inventions/tag/{tag}")
    public ResponseEntity<List<Invention>> getInventionsByTag(@PathVariable(PARAM_INVENTION_TAG) String tag) {
        List<Invention> toSend = new ArrayList<>();
        for (Invention i : inventions) {
            if (i.getTags().contains(tag))
                toSend.add(i);
        }
        return new ResponseEntity<>(toSend, HttpStatus.OK);
    }

    @GetMapping(value = "/inventions/{id}/discovery")
    public ResponseEntity<Invention> discoverInventionTagMatch(@PathVariable(PARAM_INVENTION_ID) Integer id) {
        Invention i = findById(id);
        Invention ret = discoverTagMatch(i);
        if (ret != null)
            return new ResponseEntity<>(ret, HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping(value = "/inventions/init")
    public ResponseEntity<String> initInventionsSet(@RequestBody List<Invention> initListOfInventions) {
        inventions.clear();
        Boolean status = inventions.addAll(initListOfInventions);
        if (status)
            return new ResponseEntity<>(RESPONSE_MSG_SUCCESS, HttpStatus.OK);
        return new ResponseEntity<>(RESPONSE_MSG_FAIL, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PostMapping(value = "/inventions")
    public ResponseEntity<String> addOneInvention(@RequestBody Invention invention) {
        Boolean status = inventions.add(invention);
        if (status)
            return new ResponseEntity<>(RESPONSE_MSG_SUCCESS, HttpStatus.OK);
        return new ResponseEntity<>(RESPONSE_MSG_FAIL, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @DeleteMapping(value = "/inventions/{id}")
    public ResponseEntity<Invention> deleteOneInventionById(@PathVariable(PARAM_INVENTION_ID) Integer id) {
        Invention i = findById(id);
        if (i != null) {
            if (inventions.remove(i))
                return new ResponseEntity<>(HttpStatus.OK);

            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    private Invention findById(Integer id) {
        for (Invention i : inventions) {
            if (i.getId().equals(id)) {
                return i;
            }
        }
        return null;
    }

    private Invention discoverTagMatch (Invention i) {
        int nbMatches = 0;
        Invention ret = null;
        for (Invention inv : inventions) {
            if(inv.getId().equals(i.getId())) {
                System.out.println("++++" + inv.getId());
                continue;
            }

            int matchCounter = 0;
            for (String tag : i.getTags()){
                if(inv.getTags().contains(tag))
                    matchCounter++;
            }

            if (matchCounter > nbMatches) {
                nbMatches = matchCounter;
                ret = inv;
                System.out.println(ret.getId());
            }
        }
        return ret;
    }
}
