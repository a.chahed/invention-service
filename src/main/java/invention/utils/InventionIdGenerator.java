package invention.utils;

public class InventionIdGenerator {
    public static Integer lastValue = 0;

    public static Integer nextValue() {
        return (lastValue++);
    }
}
