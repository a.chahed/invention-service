package invention.controller;

import invention.entities.Invention;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@FixMethodOrder()
public class InventionControllerTest {

    private static final Integer ID_TO_TEST = 4;
    InventionController toTest = new InventionController();
    List<Invention> inventions = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        Invention proto = new Invention();
        List<String> protoTags = new ArrayList<>();

        proto.setDate(1990);
        proto.setInventor("John Doe");
        proto.setName("Test 01");
        proto.setOrigin("Origin 01");
        protoTags.add("Baa");
        protoTags.add("Boo");
        protoTags.add("Bee");
        proto.setTags(protoTags);
        protoTags.clear();
        inventions.add(proto);

        proto.setDate(1980);
        proto.setInventor("Jane Doe");
        proto.setName("Test 02");
        proto.setOrigin("Origin 02");
        protoTags.add("Baa");
        protoTags.add("Buu");
        protoTags.add("Bee");
        proto.setTags(protoTags);
        protoTags.clear();
        inventions.add(proto);

        proto.setDate(1970);
        proto.setInventor("Luc Doe");
        proto.setName("Test 03");
        proto.setOrigin("Origin 03");
        protoTags.add("Caa");
        protoTags.add("Boo");
        protoTags.add("Cee");
        proto.setTags(protoTags);
        protoTags.clear();
        inventions.add(proto);

        proto.setDate(1960);
        proto.setInventor("Foo Doe");
        proto.setName("Test 04");
        proto.setOrigin("Origin 04");
        protoTags.add("Baa");
        protoTags.add("Boo");
        protoTags.add("Cee");
        proto.setTags(protoTags);
        protoTags.clear();
        inventions.add(proto);
    }

    @Test
    @Order(1)
    public void initInventionsSet() {
        ResponseEntity resp = toTest.initInventionsSet(inventions);
        assert (resp.getStatusCode().equals(HttpStatus.OK));
    }

    /* The ordering of the tests execution fails
    @Test
    @Order(2)
    public void getInventions() {
        assertEquals(this.inventions.size(), toTest.getInventions().getBody().size());
    }
    */

    @Test
    @Order(3)
    public void getOneInventionById() {
        assertNotNull(toTest.getOneInventionById(ID_TO_TEST));
    }

    @Test
    @Order(4)
    public void addOneInventionResponseStatus() {
        Invention proto = new Invention();
        List<String> protoTags = new ArrayList<>();
        proto.setDate(1990);
        proto.setInventor("Bruce Wayne");
        proto.setName("The extra one");
        proto.setOrigin("Origin 99");
        protoTags.add("Baa");
        protoTags.add("Coo");
        protoTags.add("Bee");
        proto.setTags(protoTags);
        protoTags.clear();
        ResponseEntity resp = toTest.addOneInvention(proto);
        assert (resp.getStatusCode().equals(HttpStatus.OK));
    }

    @Test
    @Order(5)
    public void addOneInvention() {
        Invention proto = new Invention();
        List<String> protoTags = new ArrayList<>();
        proto.setDate(1990);
        proto.setInventor("Bruce Wayne");
        proto.setName("The extra one");
        proto.setOrigin("Origin 99");
        protoTags.add("Baa");
        protoTags.add("Coo");
        protoTags.add("Bee");
        proto.setTags(protoTags);
        protoTags.clear();
        ResponseEntity resp = toTest.addOneInvention(proto);
        assert (toTest.getInventions().getBody().contains(proto));
    }

    @Test
    public void deleteOneInventionById() {
    }

    @Test
    public void getInventionsByTag() {
        List<Invention> resp = toTest.getInventionsByTag("Baa").getBody();
        assertEquals(2, resp.size());
    }
}