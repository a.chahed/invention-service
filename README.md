# Invention Service

This project consists in an inventions managing webservices, exposes simple CRUD (Create, Read, Update, Delete)

## Getting Started

This module is written in Java using SpringBoot framework.

### Prerequisites

This project has been developped using the following tools in these versions:
 - jdk-1.8.0
 - Maven 3.5.3

Running these commands on terminal should you the versions installed:
For JDK
```
java -version
```

For Maven 
```
mvn -version
```

If not, please refer to the Installing section

### Installing

First of all download jdk-1.8.0 or newer.
Add the jdk folder somewhere accessible like:
Windows:
```
C:\User\{{your-user-account}}
```
Linux:
```
/home/{{your-user-account}}/
```

Then add the path to that folder as environment variable under the path name JAVA_HOME and add it in the PATH value.